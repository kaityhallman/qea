Rails.application.routes.draw do
  root 'events#index'

  resources :events, only: [:show, :index]
  resources :info_forms

  post "/" => 'info_forms#create'

  namespace :admin do
    resources :events, :packages
  end
end
