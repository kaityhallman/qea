class AddEventIdToPackage < ActiveRecord::Migration
  def change
    add_column :packages, :event_id, :integer
  end
end
