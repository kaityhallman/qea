class InfoFormsController < ApplicationController

  def create
    @info_form = InfoForm.new(info_form_params)

    respond_to do |format|
    	if @info_form.save
    		format.html { redirect_to root_url, notice: 'Information saved.' }
      end
    end
  end

  private

  def info_form_params
  	params.require(:info_form).permit(:first_name, :last_name, :email)
  end
end
