class EventsController < ApplicationController
  def show
    @event = Event.find params[:id]
    @packages = Package.where(event_id: @event.id)
    @package = Package.find params[:id] rescue nil
    @info_form = InfoForm.new
  end

  def index
    @events = Event.all.order("starts_on ASC").where(active: true)
  end
end
