class Event < ActiveRecord::Base
	has_many :packages

	def difference
		today = Date.today

		start_date = starts_on.to_date

		if start_date > today
			difference = (start_date - today).to_i.to_s + " days"
		else
			difference = "The event has passed"
		end
	end
end
